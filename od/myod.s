	.file	"myod.c"
	.section	.rodata
.LC0:
	.string	"r+"
.LC1:
	.string	"b"
.LC2:
	.string	"  \\n"
.LC3:
	.string	"%3c"
.LC4:
	.string	"%3x"
.LC5:
	.string	"%-3x"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$1040, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$.LC0, %esi
	movl	$.LC1, %edi
	call	fopen
	movq	%rax, -1016(%rbp)
	movl	$1, -1028(%rbp)
	movl	$1, -1024(%rbp)
	movl	$0, -1020(%rbp)
	jmp	.L2
.L3:
	movl	-1028(%rbp), %eax
	cltq
	movzbl	-1029(%rbp), %edx
	movb	%dl, -1008(%rbp,%rax)
	addl	$1, -1028(%rbp)
.L2:
	movq	-1016(%rbp), %rax
	movq	%rax, %rdi
	call	fgetc
	movb	%al, -1029(%rbp)
	cmpb	$-1, -1029(%rbp)
	jne	.L3
	movl	-1028(%rbp), %eax
	movl	%eax, -1020(%rbp)
	movl	$1, -1028(%rbp)
	jmp	.L4
.L10:
	movl	-1028(%rbp), %eax
	cltq
	movzbl	-1008(%rbp,%rax), %eax
	cmpb	$10, %al
	jne	.L5
	movl	$.LC2, %edi
	movl	$0, %eax
	call	printf
	jmp	.L6
.L5:
	movl	-1028(%rbp), %eax
	cltq
	movzbl	-1008(%rbp,%rax), %eax
	movsbl	%al, %eax
	movl	%eax, %esi
	movl	$.LC3, %edi
	movl	$0, %eax
	call	printf
.L6:
	movl	-1028(%rbp), %eax
	andl	$15, %eax
	testl	%eax, %eax
	jne	.L7
	movl	$10, %edi
	call	putchar
	jmp	.L8
.L9:
	movl	-1024(%rbp), %eax
	cltq
	movzbl	-1008(%rbp,%rax), %eax
	movsbl	%al, %eax
	movl	%eax, %esi
	movl	$.LC4, %edi
	movl	$0, %eax
	call	printf
	addl	$1, -1024(%rbp)
.L8:
	movl	-1024(%rbp), %eax
	cmpl	-1028(%rbp), %eax
	jle	.L9
	movl	$10, %edi
	call	putchar
.L7:
	addl	$1, -1028(%rbp)
.L4:
	movl	-1028(%rbp), %eax
	cmpl	-1020(%rbp), %eax
	jle	.L10
	jmp	.L11
.L12:
	movl	-1024(%rbp), %eax
	cltq
	movzbl	-1008(%rbp,%rax), %eax
	movsbl	%al, %eax
	movl	%eax, %esi
	movl	$.LC5, %edi
	movl	$0, %eax
	call	printf
	addl	$1, -1024(%rbp)
.L11:
	movl	-1028(%rbp), %eax
	subl	$2, %eax
	cmpl	-1024(%rbp), %eax
	jge	.L12
	movl	$10, %edi
	call	putchar
	movq	-1016(%rbp), %rax
	movq	%rax, %rdi
	call	fclose
	movl	$0, %eax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L14
	call	__stack_chk_fail
.L14:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
