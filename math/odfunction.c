int odfunction(int num, char data[])
{
    int i,j;
    for(i=0; i<num; i=i+4)
    {
        if(i%16==0)
        {
            printf("\n%07o\t\t", i);
        }
        printf(" %02x%02x%02x%02x\t", data[i+3], data[i+2], data[i+1], data[i]);
        if((i+4)%16==0)
        {
            printf("\n\t  ");
            for (j = i - 12; j < i+4 ; j++)
            {
                if ((int)data[j] == 10)
                {
                    printf(" \\");
                    printf("n ");
                }
                else
                {
                    printf("  %c ", data[j]);
                }
            }
        }
        if(data[i+4]==0)
        {
            printf("\n\t   ");
            for(j = i-i%16; data[j-3] != 0; j++)
            {
                if((int)data[j] == 10)
                {
                    printf(" \\");
                    printf("n  ");
                }
                else
                {
                    printf(" %c ", data[j]);
                }
            }
            break;
        }
    }
    printf("\n%07o\n", i+2 );
}
